/*
 * Lover.java
 * The class that outlines a basic lover object
 * Copyright © 2015 Emmauel Singletary under the MIT License.
 * Please see http://opensource.org/licenses/MIT for more info.
 */
package co.manny.love;

/**
 * Lover
 *
 * @author Manny
 */
public class Lover {

    private String name;
    private int age, gender;

    // Gender types are 0 = Male, 1 = Female, 2 = Other (LGBT pls)
    public Lover() {
        name = "";
        age = -1;
        gender = -1;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getGender() {
        return gender;
    }

    public void setName(String s) {
        name = s;
    }

    public void setAge(int i) {
        age = i;
    }

    public void setGender(int i) {
        if (i >= 2 || i < 0) {
            System.out.println("Invalid gender, please use 0-2.");
        } else {
            gender = i;
        }
    }
}
