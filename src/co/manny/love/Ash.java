/*
 * Ash.java
 * This is the main class for this program. I'm not quite sure what my goal is yet, but we'll see.
 * This sections is thus subject to change.
 * Copyright © 2015 Emmauel Singletary under the MIT License.
 * Please see http://opensource.org/licenses/MIT for more info.
 */
package co.manny.love;

import java.util.Scanner;

/**
 * Ash
 *
 * @author Manny
 */
public class Ash {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // setup our input stream for console keyboard input.
        Scanner keyboardInput = new Scanner(System.in);

        String lover1, lover2;

        System.out.print("Enter the first name: ");
        lover1 = keyboardInput.nextLine();
        System.out.print("Now for the second one: ");
        lover2 = keyboardInput.nextLine();
      
        System.out.println(lover1 + " is very compatable with " + lover2 + ". Mmm so steamy.");
        System.out.println("Happy pi day");
    }
}
